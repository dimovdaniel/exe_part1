import React from 'react';
import './App.css';
import firebase from 'firebase';

import Header from './components/Header';
import Hero from './components/Hero';
import Member from './components/Member';
import Product from './components/Product';
import Promo from './components/Promo';



const firebaseConfig = {
  apiKey: "AIzaSyBhqKMqEQ5w2PV2DmwnIioT2lF68O_vuKo",
  authDomain: "execonfsk.firebaseapp.com",
  databaseURL: "https://execonfsk.firebaseio.com",
  projectId: "execonfsk",
  storageBucket: "execonfsk.appspot.com",
  messagingSenderId: "729433001839",
  appId: "1:729433001839:web:01df871387df23f2e9229a"
};



class App extends React.Component {

  constructor(props){
    super(props);

    this.state={
        products:{
            
        },
        members:{
        }
    }
  }

  /**
   * LifeCycles
   *  
   */
  componentDidMount(){
    
    //Bind
    this.fetchDataAndSetAt=this.fetchDataAndSetAt.bind(this);

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

    //Fetch Data

    //Products
    this.fetchDataAndSetAt("appadmin/products","products");

    //Members
    this.fetchDataAndSetAt("appadmin/member","members");
  }
  

  //Data fetcher
  /**
   * Fetch data from firebase and set at position
   * @param {String} path 
   * @param {String} variableName 
   */
  fetchDataAndSetAt(path,variableName){
    var _this=this;
    firebase.database().ref(path).on('value', function(snapshot) {
        var objToSave={};
        objToSave[variableName]=snapshot.val();
        _this.setState(objToSave);
    });
  }

  render() {
    return (
      <div className="App">
         <Header />
         <main>
            <Hero />
            <section class="section section-lg" id="products">
                {
                    Object.keys(this.state.products).map((key)=>{
                        return <Product  name={this.state.products[key]['name']} link={this.state.products[key]['link']}  description={this.state.products[key]['description']} image={this.state.products[key]['image']}  />
                    })
                }
                
            </section>
            <section class="section section-lg" id="team">
            <div className="container">
            <div className="row justify-content-center text-center mb-lg">
                <div className="col-lg-8">
                <h2 className="display-3">The amazing Team</h2>
                <p className="lead text-muted">Young and Vibrant. 2 main characteristics of the Mobidonia Team.</p>
                </div>
            </div>
            <div className="row">

                {
                    Object.keys(this.state.members).map((key)=>{
                        return <Member  name={this.state.members[key]['name']}   position={this.state.members[key]['position']} image={this.state.members[key]['image']}  social={this.state.members[key]['social']} />
                    })
                }
                
            </div>
            </div>
            </section>
            <Promo />
         </main>
         <footer>

         </footer>
       
      </div>
    )
}
}


export default App;
