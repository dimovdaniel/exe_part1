import React from 'react';
import logo from './logo.svg';
import './App.css';
import firebase from 'firebase';


const firebaseConfig = {
  apiKey: "AIzaSyBhqKMqEQ5w2PV2DmwnIioT2lF68O_vuKo",
  authDomain: "execonfsk.firebaseapp.com",
  databaseURL: "https://execonfsk.firebaseio.com",
  projectId: "execonfsk",
  storageBucket: "execonfsk.appspot.com",
  messagingSenderId: "729433001839",
  appId: "1:729433001839:web:01df871387df23f2e9229a"
};



class App extends React.Component {

  constructor(props){
    super(props);

    this.state={
      message:"Hello ExeConf, We Love you",
      messages:{
        1:{
          text:"Daniel"
        }
      },
      typedMessage:""
    }
  }

  /**
   * LifeCycles
   *  
   */
  componentDidMount(){
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    

    //Bind
    this.handleChange=this.handleChange.bind(this);
    this.printMessages=this.printMessages.bind(this);
    this.onSubmit=this.onSubmit.bind(this);
    this.getChatMessage=this.getChatMessage.bind(this);

    //Get messages
    this.getChatMessage();
    this.getWelcomeMessage();
  }

  /**
   * Data fetchers
   */

   /**
    * Retrives welcome message from firebase
    */
  getWelcomeMessage(){
    var _this=this;
    firebase.database().ref('message').on('value', function(snapshot) {
      _this.updateMessage(snapshot.val());
    });
  }

  /**
   * Reads chats messages from Firebase
   */
  getChatMessage(){
    var _this=this;
    firebase.database().ref('messages').on('value', function(snapshot) {
      _this.setState({messages:snapshot.val()});
    });
  }

  /**
   * Data savers
   */
  
   /**
   * Push message in firebase
   * @param {Number} index 
   * @param {String} message 
   */
  saveMessage(index,message){
    firebase.database().ref('messages/' + index).set({
      text: message,
    });
  }

  /**
   * State editors
   */

   /**
    * Updates the message in state
    * @param {String} message 
    */
  updateMessage(message){
    this.setState({
      message:message,
      typedMessage:"",
    })
  }


  /**
   * FORM Handlers
   */
  handleChange(event){
    var theMessage=event.target.value;
    this.setState({
      typedMessage:theMessage
    })
  }

  /**
   * When user click on the submit button
   */
  onSubmit(){
    var typedMessage=this.state.typedMessage;
    this.setState({
      typedMessage:""
    })

    this.saveMessage((new Date()).getTime(),typedMessage)
  }


  /**
   * Printers
   */
  
   /**
   * Single chat message
   * @param {Object} element 
   */
  printMessage(element){
    return (<div className="alert alert-primary" role="alert">
      {element.text}
  </div>)
  }

  /**
   * Prints all chat messages
   */
  printMessages(){
    return (<div>
      {
        Object.keys(this.state.messages).map((key)=>{
          return this.printMessage(this.state.messages[key])
        })
      }
    </div>)
   
  }
  render() {
    return (
      <div className="App">
         <header className="header-global">

         </header>
         <main>
         <section className="section section-lg pt-lg-0 section-contact-us">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-8">
                <div className="card bg-gradient-secondary shadow">
                  <div className="card-body p-lg-5">
                    <h4 className="mb-1">{this.state.message}</h4>

                    <hr />
                    {this.printMessages()}
                    <hr />
                    
                    
                    <br />
                    <div className="form-group mb-4">
                      <textarea className="form-control form-control-alternative" name="name" rows={4} cols={80} placeholder="Type a message..."  value={this.state.typedMessage} onChange={this.handleChange} />
                    </div>
                    <div>
                      <a onClick={this.onSubmit} href="#" type="button" className="btn btn-default btn-round btn-block btn-lg">Send Message</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
         </main>
         <footer>

         </footer>
       
      </div>
    )
}
}


export default App;
