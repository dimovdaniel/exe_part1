import React from 'react';
class Hero extends React.Component {
    render() {
      return (
  
        <div className="position-relative">
          {/* shape Hero */}
          <section className="section section-lg section-shaped pb-250">
            <div className="shape shape-style-1 shape-default">
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
            </div>
            <div className="container py-lg-md d-flex">
              <div className="col px-0">
                <div className="row">
                  <div className="col-lg-6">
                    <h1 className="display-3  text-white">Mobidonia<span>CodeLess Solutions.</span></h1>
                    <p className="lead  text-white">Team of three developers and one designer whose goal is to provide you with solutions that requires no programing knowledge</p>
                  </div>
                </div>
              </div>
            </div>
            {/* SVG separator */}
            <div className="separator separator-bottom separator-skew">
              <svg x={0} y={0} viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <polygon className="fill-white" points="2560 0 2560 100 0 100" />
              </svg>
            </div>
          </section>
          {/* 1st Hero Variation */}
        </div>
      );
    }
  }
  export default Hero;