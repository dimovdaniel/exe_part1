import React from 'react';
class Header extends React.Component {
    render() {
      return (
  
        <header className="header-global">
          <nav id="navbar-main" className="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light headroom">
            <div className="container">
              <a className="navbar-brand mr-lg-5" href="#">
                <img src="http://mobidonia.com/assets/img/brand/white.png" alt="brand" />
              </a>
              <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon" />
              </button>
              <div className="navbar-collapse collapse" id="navbar_global">
                <div className="navbar-collapse-header">
                  <div className="row">
                    <div className="col-6 collapse-brand">
                      <a href="#">
                        <img src="http://mobidonia.com/assets/img/brand/white.png" alt="brand" />
                      </a>
                    </div>
                    <div className="col-6 collapse-close">
                      <button type="button" className="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                        <span />
                        <span />
                      </button>
                    </div>
                  </div>
                </div>
                <ul className="navbar-nav navbar-nav-hover align-items-lg-center">
                  <li className="nav-item ">
                    <a href="#products" className="nav-link" role="button">
                      <i className="ni ni-ui-04 d-lg-none" />
                      <span className="nav-link-inner--text">Products</span>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="#team" className="nav-link" role="button">
                      <i className="ni ni-collection d-lg-none" />
                      <span className="nav-link-inner--text">Team</span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </header>
      );
    }
  }

  export default Header;