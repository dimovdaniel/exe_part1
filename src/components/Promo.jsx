import React from 'react';
class Promo extends React.Component {
    render() {
      return (
        <section className="section section-lg pt-0">
  <div className="container">
    <div className="card bg-gradient-warning shadow-lg border-0">
      <div className="p-5">
        <div className="row align-items-center">
          <div className="col-lg-8">
            <h3 className="text-white">Manage Content For Any Platform</h3>
            <p className="lead text-white mt-3">Headless CMS for Digital Creators. Power up your Firebase and Firestore with our CMS tool</p>
          </div>
          <div className="col-lg-3 ml-lg-auto">
            <a href="https://appadm.in" className="btn btn-lg btn-block btn-white">Start Now</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
    
      )
    }
}
export default Promo;