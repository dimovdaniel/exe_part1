import React from 'react';
class Member extends React.Component {

    printSocialLink(key,link){
        return (
            <a href={link} className="btn btn-warning btn-icon-only rounded-circle">
            <i className={"fa fa-"+key}  />
          </a>
        )
    }

    render() {
      return (
        <div className="col-md-6 col-lg-3 mb-5 mb-lg-0">
  <div className="px-4">
    <img src={this.props.image}  className="rounded-circle img-center img-fluid shadow shadow-lg--hover" style={{width: '200px'}} alt="image" />
    <div className="pt-4 text-center">
      <h5 className="title">
        <span className="d-block mb-1">{this.props.name} </span>
        <small className="h6 text-muted">{this.props.position} </small>
      </h5>
      <div className="mt-3">
          {
              Object.keys(this.props.social?this.props.social:[]).map((key)=>{
                    return this.printSocialLink(this.props.social[key].type,this.props.social[key].link);
              })
          }
      </div>
    </div>
  </div>
</div>
     
      )
    }
}
export default Member;