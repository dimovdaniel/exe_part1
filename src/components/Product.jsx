import React from 'react';
class Product extends React.Component {
    render() {
      return (
        <div className="container">
        <div className="row row-grid align-items-center">
          <div className="col-md-6 order-md-2">
            <img src={this.props.image} className="img-fluid floating" alt="image" />
          </div>
          <div className="col-md-6 order-md-1">
            <div className="pr-md-5">
               <h3>{this.props.name}</h3>
              <p>{this.props.description}</p>
              <p><a href={this.props.link}>Visit project</a></p>
            </div>
          </div>
        </div>
      </div>      
      )
    }
}
export default Product;